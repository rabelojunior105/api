import { Router } from 'express';
import multer from 'multer';
import multerConfig from './config/multer';

import SessionController from './app/controllers/SessionController';
import authMiddware from './app/middlewares/auth';
import AdressesController from './app/controllers/AdressesController';
import UserController from './app/controllers/UserController';
import FileController from './app/controllers/FileController';

const routes = new Router();
const upload = multer(multerConfig);
// Session e cadastro dos Usuarios
routes.post('/session', SessionController.store);

routes.post('/users', UserController.store);
// Middware para testar se o usuario está com token
routes.use(authMiddware);

// TESTES: OK
routes.post('/files', upload.single('file'), FileController.store);

// TESTES: OK
routes.post('/adresses', AdressesController.store);

routes.put('/users', UserController.update);

// TESTES: OK

export default routes;
