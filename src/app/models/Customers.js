import Sequelize, { Model } from 'sequelize';
class Customers extends Model {
  static init(sequelize) {
    super.init(
      {
        sexo: Sequelize.STRING,
        birthday: Sequelize.DATE,
        telephone: Sequelize.STRING,
        rating: Sequelize.STRING,
        status: Sequelize.BOOLEAN,        
      },
      {
        sequelize,
      }
      
    );

    return this;
  }
  static associate(models) {
    this.belongsTo(models.File, {
      foreignKey: 'fk_avatar',
      as: 'avatar',
    });
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'users',
    });
    this.belongsTo(models.Adresses, {
      foreignKey: 'fk_addresses',
      as: 'addresses',
    });
  }

}

export default Customers;



