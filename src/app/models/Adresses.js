import Sequelize, { Model } from 'sequelize';

class Adresses extends Model {
  static init(sequelize) {
    super.init(
      {
        state: Sequelize.STRING,
        city: Sequelize.STRING,
        neighborhood: Sequelize.STRING,
        street: Sequelize.STRING,
        number: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
}

export default Adresses;
