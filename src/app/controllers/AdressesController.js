import * as Yup from 'yup';
import Adresses from '../models/Adresses';

class AdressesController {
  async store(req, res) {
    const schema = Yup.object().shape({
      state: Yup.string().required(),
      city: Yup.string().required(),
      neighborhood: Yup.string().required(),
      street: Yup.string().required(),
      number: Yup.string().required(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validação inválida' });
    }

    const {
      id,
      number,
      state,
      neighborhood,
      city,
      street,
    } = await Adresses.create(req.body);
    return res.json({ id, number, state, neighborhood, city, street });
  }

  async findAll(req, res) {
    const adresses = await Adresses.findAll();
    return res.json(adresses);
  }

  async update(req, res) {
    const { id } = req.body;

    const schema = Yup.object().shape({
      state: Yup.string(),
      city: Yup.string(),
      neighborhood: Yup.string(),
      street: Yup.string(),
      number: Yup.string(),
    });
    const addresses = await Adresses.findByPk(id);
    if (!(await schema.isValid(req.body))) {
      return res.status(400).json({ error: 'Validação inválida' });
    }
    if (id !== Adresses.id) {
      const AdressesExists = await Adresses.findOne({
        where: { id },
      });
      if (!AdressesExists) {
        return res.status(400).json({ error: 'Endereço inexistente' });
      }
    }

    const {
      number,
      state,
      neighborhood,
      city,
      street,
    } = await addresses.update(req.body);
    return res.json({ id, number, state, neighborhood, city, street });
  }
}

export default new AdressesController();
