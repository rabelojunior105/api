import Sequelize from 'sequelize';
import mongoose from 'mongoose';
import db from '../config/database';

import User from '../app/models/Users';
import File from '../app/models/Files';
import Adresses from '../app/models/Adresses';
import Customers from '../app/models/Customers';

const models = [User, File, Adresses, Customers];

class Database {
  constructor() {
    this.init();
    this.mongo();
  }

  init() {
    this.connection = new Sequelize(db);

    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }

  mongo() {
    this.mongoConnection = mongoose.connect(
      'mongodb://localhost:27017/apiPM',
      {
        useNewUrlParser: true,
        useFindAndModify: true,
        useUnifiedTopology: true,
      }
    );
  }
}

export default new Database();
