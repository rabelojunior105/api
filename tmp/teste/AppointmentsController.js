import * as Yup from 'yup';
import { startOfHour, parseISO, isBefore, format, subHours } from 'date-fns';
import pt from 'date-fns/locale/pt-BR';
import Users from '../models/Users';
import Files from '../models/Files';
import Notification from '../schemas/notification';
import Appointments from '../models/Appointments';
import Queue from '../../lib/Queue';
import CancellationMailer from '../jobs/CancellationMailer';

class AppointmentsController {
  async index(req, res) {
    const { page = 1 } = req.query;
    const appointment = await Appointments.findAll({
      where: { fk_user: req.userId, canceled_at: null },
      order: ['date'],
      limit: 20,
      offset: (page - 1) * 20,
      attributes: ['id', 'date'],
      include: [
        {
          model: Users,
          as: 'provider',
          attributes: ['id', 'name'],
          include: [
            {
              model: Files,
              as: 'avatar',
              attributes: ['id', 'path', 'url'],
            },
          ],
        },
      ],
    });
    return res.json(appointment);
  }

  async store(req, res) {
    const schema = Yup.object().shape({
      fk_provider: Yup.number().required(),
      date: Yup.date().required(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.status(500).json({ error: ' validação Invalid' });
    }
    const { fk_provider, date } = req.body;
    if (fk_provider === req.userId) {
      return res.status(401).json('provider is not can match user admin');
    }
    /**
     * check if fk_provider is a provider
     */
    const isProvider = await Users.findOne({
      where: { id: fk_provider, provider: true },
    });
    if (!isProvider) {
      return res
        .status(401)
        .json({ error: 'you can only create appointment providers' });
    }
    const hourStart = startOfHour(parseISO(date));
    /**
     * check for past dates
     */
    if (isBefore(hourStart, new Date())) {
      return res.status(400).json({ error: 'Past date are not permitted' });
    }

    /**
     * check date availability
     */
    const checkAvailability = await Appointments.findOne({
      where: {
        fk_provider,
        canceled_at: null,
        date: hourStart,
      },
    });
    if (checkAvailability) {
      return res.status(400).json({ error: 'Appointment is not available' });
    }

    const appointments = await Appointments.create({
      fk_user: req.userId,
      fk_provider,
      date: hourStart,
    });

    /**
     * Notify appointment provider
     */
    const user = await Users.findByPk(req.userId);
    const formatteDate = format(hourStart, "'dia' dd 'de' MMMM', ás' H:mm'h'", {
      locale: pt,
    });
    await Notification.create({
      content: `Novo agendamento de ${user.name} para ${formatteDate}`,
      fk_user: fk_provider,
    });
    return res.json(appointments);
  }

  async delete(req, res) {
    const appointment = await Appointments.findByPk(req.params.id);
    const users = await Users.findByPk(appointment.fk_user);
    const provider = await Users.findByPk(req.userId);
    if (appointment.fk_user !== req.userId) {
      return res.status(401).json({
        error: "You dont't have permissiont o cancel this appointments",
      });
    }

    const dateWithSub = subHours(appointment.date, 2);

    if (isBefore(dateWithSub, new Date())) {
      return res.status(401).json({
        error: 'You can only cancel appointments 2 hours in advance.',
      });
    }
    appointment.canceled_at = new Date();

    await appointment.save();

    await Queue.add(CancellationMailer.key, {
      appointment,
      users,
      provider,
    });

    return res.json(appointment);
  }
}
export default new AppointmentsController();
