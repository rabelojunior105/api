import Notification from '../schemas/notification';
import Users from '../models/Users';

class NotificationController {
  async index(req, res) {
    const isProvider = await Users.findOne({
      where: { id: req.userId, provider: true },
    });
    if (!isProvider) {
      return res
        .json(401)
        .json({ error: 'Only provider can load notifications' });
    }
    const notification = await Notification.find({
      fk_user: req.userId,
    })
      .sort({ createdAt: 'desc' })
      .limit(20);

    return res.json(notification);
  }

  async update(req, res) {
    const notification = await Notification.findByIdAndUpdate(
      req.params.id,
      { read: true },
      { new: true }
    );
    return res.json(notification);
  }
}
export default new NotificationController();
